<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 20:21
 */

?>


<html>
<head>
    <title>DragonsGetIt | YTView Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<h1>DragonsGetIt | YouTubeView Admin</h1><br>

<div id="support">
    <style>
        #support {
            position: fixed;
            top: 15px;
            right: 15px;
        }

        .lock-1 {
            color: red;
        }
    </style>
    <i><b>Made by Aresak</b><br>
        for DragonsGetIt.com</i><br>
    <i>
        <small>Waddap <?php echo $user->fullname ?></small>
    </i>

    <script>
        String.prototype.replaceAt=function(index, replacement) {
            return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
        }
        function lock_playlist(id) {
            $.post("admin.ajax.php", {method: "lock-pl", id: id}, function(data) {console.log(data);loadPlaylists();}, "html");
        }

        function startup() {
            loadPending();
            loadPlaylists();
            loadVideos();
        }

        function loadPending() {
            $("#admin-pending").html("<tr id=\"load-pending\"><td align=\"center\"><span class=\"loading\"><img src=\"https://m.popkey.co/163fce/Llgbv_s-200x150.gif\"></span></td></tr>");
            $.post("admin.ajax.php", {method: "show_pending"}, function(data) {$("#admin-pending").html(data);}, "html");
        }

        function loadPlaylists() {
            $("#playlists").html("<tr id=\"load-pending\"><td align=\"center\"><span class=\"loading\"><img src=\"https://m.popkey.co/163fce/Llgbv_s-200x150.gif\"></span></td></tr>");
            $.post("admin.ajax.php", {method: "show_playlists"}, function(data) {$("#playlists").html(data);}, "html");
        }

        function loadVideos() {
            $("#videos").html("<tr id=\"load-pending\"><td align=\"center\"><span class=\"loading\"><img src=\"https://m.popkey.co/163fce/Llgbv_s-200x150.gif\"></span></td></tr>");
            $.post("admin.ajax.php", {method: "show_videos"}, function(data) {$("#videos").html(data);}, "html");
        }

        function toolMe(x) {
            $("#toolbox-connected").text("FALSE");
            $("#toolbox-id").text($("#user-" + x + "-id").text());
            $("#toolbox-username").text($("#user-" + x + "-username").text());
            $("#toolbox-email").text($("#user-" + x + "-email").text());
            $("#toolbox-twitch").text($("#user-" + x + "-twitch").text());
            $("#toolbox-youtube").text($("#user-" + x + "-youtube").text());
            $("#toolbox").show();

            var j = $("#toolbox-youtube").text();
            var k = j.split("|")[0].split("/");
            switch(k[3]) {
                case "channel":
                    testfor("playlist", k[4].replaceAt(1, "U"));
                    break;
                case "c":
                case "user":
                    testfor("user", k[4]);
                    break;
                default:
                    testfor("user", k[3]);
                    break;
            }
        }

        function testfor(x, y) {
            $.post("admin.ajax.php", {method: "testfor", type: x, input: y}, function(data) {
                var json = JSON.parse(data);
                if(json["error"] == false) {
                    $("#toolbox-playlistId").val(json["playlist"]);
                } else {

                }

                $("#toolbox-ajax").html(data);
                $("#toolbox-connected").html(json["connected"]);
            }, "html");
        }

        function approve() {
            $("#load-approve").show();
            $.post("admin.ajax.php", {method: "approve", id: $("#toolbox-id").text(), playlist: $("#toolbox-playlistId").val()}, function(data) {
                var j = JSON.parse(data);
                $("#approve-text").html("Approved " + $("#toolbox-username").text() + " and added " + j["_total"] + " his videos to our DB!");
                $("#load-approve").hide();
                $("#toolbox").hide();

                loadPending();
                loadVideos();
                loadPlaylists();
            }, "html");
        }

        setTimeout(startup, 1000);
    </script>
</div>

<div id="admin">
    <h2 class="color: green;" id="approve-text"></h2>
    <div id="toolbox" style="display: none;">
        <table class="table table-striped">
            <tr style="display: none;" id="load-approve"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
            <tr><td><h4>Item</h4></td><td><h4>Value</h4></td></tr>
            <tr><td><b>ID</b></td><td id="toolbox-id"></td></tr>
            <tr><td><b>Username</b></td><td id="toolbox-username"></td></tr>
            <tr><td><b>Email</b></td><td id="toolbox-email"></td></tr>
            <tr><td><b>Twitch</b></td><td id="toolbox-twitch"></td></tr>
            <tr><td><b>YouTube</b></td><td id="toolbox-youtube"></td></tr>
            <tr><td><b>Connected</b></td><td id="toolbox-connected">FALSE</td></tr>
            <tr><td><b>PlaylistID</b></td><td><input type="text" id="toolbox-playlistId"></td></tr>
            <tr><td></td><td><button style="background-color: forestgreen" onclick="approve();">APPROVE!</button></td></tr>
            <tr><td></td><td><button onclick="testfor('user');">Test for User</button></td></tr>
            <tr><td></td><td><button onclick="testfor('channel');">Test for Channel ID</button></td></tr>
            <tr><td></td><td><button onclick="testfor('playlist');">Test for Playlist ID</button></td></tr>
            <tr><td></td><td id="toolbox-ajax"></td></tr>
        </table>
    </div>

    <table id="admin-pending" class="table table-striped">
        <tr id="load-pending"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
    </table>


    <table id="admin-approved" style="display: none;" class="table table-striped">
        <tr id="load-approved"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
    </table>


    <table id="admin-denied" style="display: none;" class="table table-striped">
        <tr id="load-denied"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
    </table>
</div>

<div>
    <table id="playlists" class="table table-striped">
        <tr id="load-playlists"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
    </table>
</div>

<div>
    <table id="videos" class="table table-striped">
        <tr id="load-videos"><td align="center"><span class="loading"><img src="https://m.popkey.co/163fce/Llgbv_s-200x150.gif"></span></td></tr>
    </table>
</div>
</body>
</html>
