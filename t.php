<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 18:34
 */

include "aresakytmodule.php";
$aytm = new AresakYouTubeModule();

/** @var YTPlaylist $playlist YouTube Playlist*/

$i = 1;
do {
    $playlist = $aytm->GetPlaylist($i);
    if($playlist == null)
        return;

    $playlist->GetUploads();
    $newVideos = $playlist->NewVideos();

    $i ++;
}
while($playlist != null);