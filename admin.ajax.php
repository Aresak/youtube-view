<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 20:15
 */

require_once "aresakytmodule.php";



if(isset($_POST)) {
    if(isset($_POST["method"]))
        $method = $_POST["method"];
    else
        die("Undefined method");

    $aytm = new AresakYouTubeModule();
    $aytm->_createConnection();


    switch($method) {
        case "approve":
            $res = array();
            $id = $_POST["id"];
            $playlist = $_POST["playlist"];

            $insertQuery = "INSERT INTO atm_ytview_playlists (user_id, playlist_id) VALUES ('$id', '$playlist')";
            $insertResult = mysqli_query($aytm->sql, $insertQuery)
                or die(mysqli_error($aytm->sql));

            // create playlist
            $play = new YTPlaylist($aytm, $playlist, $id, 0);

            // get all uploads
            $play->GetUploads(true);

            // insert all uploads
            $play->NewVideos();

            // dump the videos
            $vids = array();
            foreach($play->videos as $video) {
                array_push($vids, array(
                    "id" => $video->videoId,
                    "title" => $video->title,
                    "published" => $video->published_at
                ));
            }
            $res["_total"] = count($vids);
            $res["videos"] = $vids;
            die(json_encode($res));
            break;
        case "testfor":
            $type = $_POST["type"];
            $input = $_POST["input"];
            $out = array();
            $out["input"] = $input;
            $out["type"] = $type;
            $out["error"] = false;
            switch($type) {
                case "user":
                    $content = file_get_contents("https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=" . $input . "&key=" . $aytm->key, true);
                    $j = json_decode($content, true);

                    if(isset($j["error"])) {
                        $out["error"] = "true";
                        $out["err-msg"] = $j["error"]["message"];
                        $out["connected"] = "<span style='color: red;'>The playlist is denied by the API! Try to set the playlist manually</span>";
                    } else {
                        $out["playlist"] = $j["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"];
                        $out["connected"] = "<span style='color: green;'>The playlist is accepted by the API! You can approve</span>";
                    }
                    break;
                case "channel":
                    $content = file_get_contents("https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=" . $input . "&key=" . $aytm->key, true);
                    $j = json_decode($content, true);

                    if(isset($j["error"])) {
                        $out["error"] = "true";
                        $out["err-msg"] = $j["error"]["message"];
                        $out["connected"] = "<span style='color: red;'>The playlist is denied by the API! Try to set the playlist manually</span>";
                    } else {
                        $out["playlist"] = $j["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"];
                        $out["connected"] = "<span style='color: green;'>The playlist is accepted by the API! You can approve</span>";
                    }
                    break;
                case "playlist":
                    $content = file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" . $input . "&key=" . $aytm->key, true);
                    $j = json_decode($content, true);

                    if(isset($j["error"])) {
                        $out["error"] = "true";
                        $out["err-msg"] = $j["error"]["message"];
                        $out["connected"] = "<span style='color: red;'>The playlist is denied by the API! Try to set the playlist manually</span>";
                    } else {
                        $out["playlist"] = $input;
                        $out["connected"] = "<span style='color: green;'>The playlist is accepted by the API! You can approve</span>";
                    }
                    break;
            }

            die(json_encode($out));
            break;
        case "lock-pl":
            $query = "SELECT * FROM atm_ytview_playlists WHERE ID='" . $_POST["id"] . "'";
            $result = mysqli_query($aytm->sql, $query)
                or die(mysqli_error($aytm->sql));
            $l = mysqli_result($result, 0, "locked");
            $query2 = "UPDATE atm_ytview_playlists SET locked='" . ($l == 0 ? 1 : 0) . "' WHERE ID='" . $_POST["id"] . "'";
            $result2 = mysqli_query($aytm->sql, $query2)
                or die(mysqli_error($aytm->sql));
            echo "done ";
            break;

        case "show_pending":
            $query = "SELECT * FROM sbr500_members WHERE youtube_url!='http://|'";
            $result = mysqli_query($aytm->sql, $query)
            or die(mysqli_error($aytm->sql));

            $users = array();
            for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                $youtube_url = mysqli_result($result, $i, "youtube_url");

                if(empty($youtube_url)) {
                    continue;
                }

                $query2 = "SELECT * FROM atm_ytview_playlists WHERE user_id='" . mysqli_result($result, $i, "id") . "'";
                $result2 = mysqli_query($aytm->sql, $query2)
                or die(mysqli_error($aytm->sql));

                if(mysqli_num_rows($result2) == 0) {
                    // we have our boi
                    array_push($users, array(
                        "ID" => mysqli_result($result, $i, "id"),
                        "username" => mysqli_result($result, $i, "username"),
                        "email" => mysqli_result($result, $i, "email"),
                        "twitch" => mysqli_result($result, $i, "twitch_url"),
                        "youtube" => mysqli_result($result, $i, "youtube_url")
                    ));
                }
            }

            ?>
            <tr>
                <td><b>ID</b></td>
                <td><b>Username</b></td>
                <td><b>Email</b></td>
                <td><b>Twitch</b></td>
                <td><b>YouTube</b></td>
            </tr>
            <?php
            foreach($users as $user) {
                ?>
                <tr id="user-<?php echo $user["ID"]; ?>" class="user-pending" onclick="toolMe('<?php echo $user["ID"]; ?>');">
                    <td id="user-<?php echo $user["ID"] . "-id"; ?>"><?php echo $user["ID"]; ?></td>
                    <td id="user-<?php echo $user["ID"] . "-username"; ?>"><?php echo $user["username"]; ?></td>
                    <td id="user-<?php echo $user["ID"] . "-email"; ?>"><?php echo $user["email"]; ?></td>
                    <td id="user-<?php echo $user["ID"] . "-twitch"; ?>"><?php echo $user["twitch"]; ?></td>
                    <td id="user-<?php echo $user["ID"] . "-youtube"; ?>"><?php echo $user["youtube"]; ?></td>
                </tr>
                <?php
            }
            break;

        case "show_playlists":
            $query = "SELECT * FROM atm_ytview_playlists";
            $result = mysqli_query($aytm->sql, $query)
                or die(mysqli_error($aytm->sql));
            ?>
            <tr>
                <td><b>ID</b></td>
                <td><b>User ID</b></td>
                <td><b>Playlist ID</b></td>
                <td><b>LOCK/UNLOCK</b></td>
            </tr>
            <?php
            for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                $l = mysqli_result($result, $i, "locked");
                $id = mysqli_result($result, $i, "ID");
                echo "<tr>";
                echo "<td class='lock-$l'>" . $id . "</td>";
                echo "<td class='lock-$l'>" . mysqli_result($result, $i, "user_id") . "</td>";
                echo "<td class='lock-$l'>" . mysqli_result($result, $i, "playlist_id") . "</td>";
                echo "<td><a href='#' onclick='lock_playlist(\"$id\")'><span class='lock " . ($l == 0 ? "glyphicon glyphicon-eye-close" : "glyphicon glyphicon-eye-open") . "'></span></a></td>";
                echo "</tr>";
            }
            break;
        case "show_videos":
            $query = "SELECT * FROM atm_ytview_videos";
            $result = mysqli_query($aytm->sql, $query)
                or die(mysqli_error($aytm->sql));

            ?>
            <tr>
                <td><b>ID</b></td>
                <td><b>Published At</b></td>
                <td><b>Title</b></td>
                <td><b>User ID</b></td>
                <td><b>Playlist ID</b></td>
                <td><b>Video ID</b></td>
                <td><b>Channel ID</b></td>
            </tr>
            <?php
            for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                $l = mysqli_result($result, $i, "locked");
                $id = mysqli_result($result, $i, "ID");
                echo "<tr>";
                echo "<td>" . mysqli_result($result, $i, "ID") . "</td>";
                echo "<td>" . mysqli_result($result, $i, "published_at") . "</td>";
                echo "<td><b>" . mysqli_result($result, $i, "title") . "</b></td>";
                echo "<td>" . mysqli_result($result, $i, "user_id") . "</td>";
                echo "<td>" . mysqli_result($result, $i, "playlist_id") . "</td>";
                echo "<td>" . mysqli_result($result, $i, "video_id") . "</td>";
                echo "<td>" . mysqli_result($result, $i, "channel_id") . "</td>";
                echo "</tr>";
            }
            break;
        default:
            die("Unknown method: " . $method);
            break;
    }
}