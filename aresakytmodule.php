<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 18:04
 */
require_once "../aresaktwitchmodule.php";


class AresakYouTubeModule {
    /** @var $key string A Google API key */
    public $key  = "";
    /** @var $base string A base url for the Google API */
    public $base    = "https://www.googleapis.com/youtube/v3/";
    /** @var $sql mysqli A MySQLi connection */
    public $sql;


    function _construct() {
        $this->_createConnection();
    }

    public function _createConnection() {
        $this->sql = atm::sql();
    }

    public function GetPlaylist($position) {
        if($this->sql == null)
            $this->_createConnection();

        $query = "SELECT * FROM atm_ytview_playlists WHERE ID='$position'";
        $result = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 1) {
            $playlist = new YTPlaylist(
                $this,
                mysqli_result($result, 0, "playlist_id"),
                mysqli_result($result, 0, "user_id"),
                mysqli_result($result, 0, "locked")
            );

            return $playlist;
        } else {
            return null;
        }
    }
}

class YTPlaylist {
    /** @var $aytm AresakYouTubeModule Module parent */
    public $aytm;
    /** @var $videos YTVideo[] The array of Video objects */
    public $videos = array();
    public $playlistId, $userId, $locked;

    function __construct($aytm, $playlistId, $userId, $locked)
    {
        $this->aytm = $aytm;
        $this->playlistId = $playlistId;
        $this->userId = $userId;
        $this->locked = $locked;

        if($this->aytm->sql == null)
            $this->aytm->_createConnection();
    }

    /** Fetch uploads from the Google API*/
    public function GetUploads($all = false) {
        if($this->aytm->sql == null)
            $this->aytm->_createConnection();

        $content = file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=" . $this->playlistId . "&key=" . $this->aytm->key, true);
        $json = json_decode($content, true);

        foreach($json["items"] as $upload) {
            $video = new YTVideo($this, $upload);
            array_push($this->videos, $video);
        }

        if($all) {
            $b = true;
            while($b) {
                if(!isset($json["nextPageToken"]))
                    $b = false;
                $token = $json["nextPageToken"];
                $content = file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&pageToken=$token&playlistId=" . $this->playlistId . "&key=" . $this->aytm->key, true);
                $json = json_decode($content, true);

                foreach($json["items"] as $upload) {
                    $video = new YTVideo($this, $upload);
                    array_push($this->videos, $video);
                }
            }
        }
    }

    /** @return YTVideo[] A new videos that have just been added to the DB */
    public function NewVideos() {
        if($this->aytm->sql == null)
            $this->aytm->_createConnection();

        $newVideos = array();
        foreach($this->videos as $video) {
            $query = "SELECT * FROM atm_ytview_videos WHERE video_id='" . $video->videoId . "'";
            $result = mysqli_query($this->aytm->sql, $query)
                or die(mysqli_error($this->aytm->sql));

            if(mysqli_num_rows($result) == 1) {
                return $newVideos;
            } else {
                array_push($newVideos, $video);
                $video->title = mysqli_escape_string($this->aytm->sql, $video->title);

                $query_insert = "INSERT INTO atm_ytview_videos (user_id, playlist_id, video_id, channel_id, title, published_at) VALUES
                  ('" . $this->userId . "', '" . $this->playlistId . "', '" . $video->videoId . "', '" . $video->channelId . "', '" . $video->title . "', '" . strtotime($video->published_at) . "')";
                $result_insert = mysqli_query($this->aytm->sql, $query_insert)
                    or die(mysqli_error($this->aytm->sql));
            }
        }

        return $newVideos;
    }
}

class YTVideo {
    /** @var $playlist YTPlaylist YouTube playlist reference */
    public $playlist;
    public $published_at, $channelId, $title, $description, $channelTitle, $position, $videoId;


    function __construct($playlist, $json) {
        $this->playlist = $playlist;
        $cur = $json["snippet"];
        $this->channelId = $cur["channelId"];
        $this->published_at = $cur["publishedAt"];
        $this->title = $cur["title"];
        $this->description = $cur["description"];
        $this->channelTitle = $cur["channelTitle"];
        $this->position = $cur["position"];
        $this->videoId = $cur["resourceId"]["videoId"];
    }

    /*
     * Returns an image link with given resolution:
     * Width:   120
     * Height:  90
     */
    public function GetThumbnailDefault() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/default.jpg";
    }

    /*
     * Returns an image link with given resolution:
     * Width:   320
     * Height:  180
     */
    public function GetThumbnailMedium() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/mqdefault.jpg";
    }

    /*
     * Returns an image link with given resolution:
     * Width:   480
     * Height:  360
     */
    public function GetThumbnailHigh() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/hqdefault.jpg";
    }

    /*
     * Returns an image link with given resolution:
     * Width:   640
     * Height:  480
     */
    public function GetThumbnailStandard() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/sddefault.jpg";
    }

    /*
     * Returns an image link with given resolution:
     * Width:   (the top quality width the video was uploaded at)
     * Height:  (the top quality height the video was uploaded at)
     */
    public function GetThumbnailMaxRes() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/maxresdefault.jpg";
    }
}
