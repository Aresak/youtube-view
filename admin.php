<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 20:21
 */

require_once "../user.injection.php";

if(isset($_GET["hidentity"])) {
    include "../aresaktwitchmodule.php";
    $sql = atm::sql();
    $ui = new userinjection();
    $user = $ui->getHidentity($sql);

    $hdr = "?hidentity=" . $_GET["hidentity"];
    $h_raw = $_GET["hidentity"];

    switch($user->usergroup_id) {
        case "1":
            $atm = new atm();
            include "pages/tpl_admin_authorized.php";
            break;
        default:
            include "pages/tpl_admin_pleb.php";
            break;
    }
}
else {
    $ui = new userinjection();
    $ui->getHeaderIdentity("TwitchScout/YouTubeView/admin.php", null);
}