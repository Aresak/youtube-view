<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 18:10
 */

?>
<html>
    <head>
        <title>YouTube View by Aresak</title>
    </head>

    <body>
        <?php


        include "aresakytmodule.php";
        $aytm = new AresakYouTubeModule();
        if(isset($_GET["port"])) {
            $port = $_GET["port"];
            switch($port) {
                case "base":
                    $rows = $_GET["rows"];
                    $cols = $_GET["cols"];

                    // messing around fix
                    if($rows > 10)
                        $rows = 10;
                    if($cols > 10)
                        $cols = 10;

                    if($rows < 1)
                        $rows = 1;
                    if($cols < 1)
                        $cols = 1;

                    $aytm->_createConnection();
                    $query = "SELECT * FROM `atm_ytview_videos` ORDER BY published_at DESC LIMIT " . ($rows * $cols);
                    $result = mysqli_query($aytm->sql, $query)
                        or die(mysqli_error($aytm->sql));

                    echo "<table>";
                    $b = 0;
                    for($row = 0; $row < $rows; $row ++) {
                        echo "<tr>";
                        for($col = 0; $col < $cols; $col ++) {
                            if($b == 0) $b = 1;
                            else $b = 0;
                            
                            $a = $row + $col;
                            $user = "SELECT * FROM sbr500_members WHERE id='" . mysqli_result($result, $a, "user_id") . "'";
                            $userR = mysqli_query($aytm->sql, $user)
                                or die(mysqli_error($aytm->sql));
                            $u = array();
                            $u["name"] = mysqli_result($userR, 0, "username");

                            echo "<td class='frame v" . $b . "' onclick='video(\"" . mysqli_result($result, $a, "video_id") . "\")'>";
                            echo "<div align='center' class='preview'><img class='previmg' src='https://i.ytimg.com/vi/" . mysqli_result($result, $a, "video_id") . "/mqdefault.jpg'></div>";
                            echo "<div class='info'><b>" . $u["name"] . "</b><br>" . mysqli_result($result, $a, "title") . "</div>";
                            echo "</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                    <style>
                        body {
                            font-family: Arial;
                        }
                        
                        td {
                            width: <?php echo (100 / $cols); ?>%;
                            height: <?php echo (100 / $rows); ?>%;
                            cursor: pointer;
                        }
                        
                        .v0 {
                            background-color: rgba(186, 198, 198, 1);
                        }
                        
                        .v1 {
                            background-color: rgba(138, 151, 151, 1);
                        }

                        .preview {
                            align-content: center;
                        }

                        table {
                            width: 100%;
                            height: 100%;
                            position absolute;
                        }
                    </style>
                    <script>
                        function video(x) {
                            window.open("https://www.youtube.com/watch?v=" + x);
                        }
                    </script>
                    <?php
                    break;
            }
        }
        ?>
    </body>
</html>
